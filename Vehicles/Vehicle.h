//
//  Vehicle.h
//  Vehicles
//
//  Created by Joan Romano on 9/7/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicle : NSObject

@property (nonatomic, readonly) NSUInteger modelYear;
@property (nonatomic, readonly) NSUInteger numberOfWheels;
@property (nonatomic, readonly, copy) NSString *powerSource;
@property (nonatomic, readonly, copy) NSString *brandName;
@property (nonatomic, readonly, copy) NSString *modelName;

+ (instancetype)vehicleWithModelYear:(NSUInteger)modelYear numberOfWheels:(NSUInteger)numberOfWheels powerSource:(NSString *)powerSource brandName:(NSString *)brandName modelName:(NSString *)modelName;

- (instancetype)initWithModelYear:(NSUInteger)modelYear numberOfWheels:(NSUInteger)numberOfWheels powerSource:(NSString *)powerSource brandName:(NSString *)brandName modelName:(NSString *)modelName;

- (NSString *)goForward;
- (NSString *)goBackward;
- (NSString *)stopMoving;
- (NSString *)makesNoise;

- (NSString *)changeGear:(NSString *)newGear;
- (NSString *)turn:(NSUInteger)degrees;

@end
