//
//  Car.m
//  Vehicles
//
//  Created by alexruperez on 10/9/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "Car.h"

@interface Car ()

@end

@implementation Car

@synthesize numberOfWheels = _numberOfWheels;
@synthesize numberOfDoors = _numberOfDoors;
@synthesize convertible = _convertible;
@synthesize hatchback = _hatchback;
@synthesize sunroof = _sunroof;

#pragma mark - Public Methods

+ (instancetype)carWithModelYear:(NSUInteger)modelYear powerSource:(NSString *)powerSource brandName:(NSString *)brandName modelName:(NSString *)modelName numberOfDoors:(NSUInteger)numberOfDoors convertible:(BOOL)convertible hatchback:(BOOL)hatchback sunroof:(BOOL)sunroof
{
    return [[self alloc] initWithModelYear:modelYear powerSource:powerSource brandName:brandName modelName:modelName numberOfDoors:numberOfDoors convertible:convertible hatchback:hatchback sunroof:sunroof];
}

- (instancetype)initWithModelYear:(NSUInteger)modelYear powerSource:(NSString *)powerSource brandName:(NSString *)brandName modelName:(NSString *)modelName numberOfDoors:(NSUInteger)numberOfDoors convertible:(BOOL)convertible hatchback:(BOOL)hatchback sunroof:(BOOL)sunroof
{
    self = [super initWithModelYear:modelYear numberOfWheels:4 powerSource:powerSource brandName:brandName modelName:modelName];
    
    if (self)
    {
        _numberOfDoors = numberOfDoors;
        _convertible = convertible;
        _hatchback = hatchback;
        _sunroof = sunroof;
    }
    
    return self;
}

- (NSString *)goForward
{
    [self start];
    NSLog(@"%@", [self changeGear:@"D"]);
    NSLog(@"Depress the gas pedal");
    return [NSString stringWithFormat:@"Vehicle go forward: %@", nil];
}

- (NSString *)goBackward
{
    [self start];
    NSLog(@"%@", [self changeGear:@"R"]);
    NSLog(@"Check your rear view mirror");
    NSLog(@"Depress the gas pedal");
    return [NSString stringWithFormat:@"Vehicle go backward: %@", nil];
}

- (NSString *)stopMoving
{
    NSLog(@"Depress the break pedal");
    NSLog(@"%@", [self changeGear:@"N"]);
    return [NSString stringWithFormat:@"Vehicle stop moving: %@", nil];
}

- (NSString *)makesNoise
{
    u_int32_t randomNoiseIndex = arc4random_uniform(3);
    NSString *randomNoise;
    
    switch (randomNoiseIndex) {
        case 0:
            randomNoise = @"BRUUUM!!!";
            break;
        case 1:
            randomNoise = @"PIPI!!!";
            break;
        default:
            randomNoise = @"CRASH!!!";
            break;
    }
    
    return [NSString stringWithFormat:@"Vehicle make noise: %@", randomNoise];
}

#pragma mark - Private Methods

- (void)start
{
    NSLog(@"Car power source has started.");
}

@end
